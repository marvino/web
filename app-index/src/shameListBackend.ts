import { z } from "zod"

export const ShameEntrySchema = z.object({
    repo: z.string(),
    errorMsg: z.string(),
})

export const ShameListSchema = z.object({
    isodatetime: z.string(),
    shame: z.array(ShameEntrySchema),
})

export type ShameType = z.infer<typeof ShameEntrySchema>
export type ShameListType = z.infer<typeof ShameListSchema>

export const parseShameListJSON = (json: any) => {
    const result = ShameListSchema.safeParse(json)

    if (result.success) {
        return result.data
    } else {
        // Handle parsing errors...
        console.error(result.error)
        return null
    }
}
