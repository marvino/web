/* @refresh reload */
import { render } from "solid-js/web"
import Home from "./pages/Home"
import AppPage from "./pages/AppPage"
import { Router, Route, Routes } from "@solidjs/router"

import "./index.css"

const root = document.getElementById("root")

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
    throw new Error(
        "Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got misspelled?"
    )
}

render(
    () => (
        <Router>
            <Routes>
                <Route path="/apps/" component={Home} />
                <Route path="/apps/app" component={AppPage} />
            </Routes>
        </Router>
    ),
    root!
)
