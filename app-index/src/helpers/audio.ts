
const audioCtx = new(window.AudioContext || window.webkitAudioContext)();

export const playDTMFSound = async (frequencyOne: number, frequencyTwo: number, duration=500) => {
  let s = new ConstantSourceNode(audioCtx);

  const audioNodeOne = audioCtx.createOscillator();
  audioNodeOne.type = 'square';
  audioNodeOne.frequency.value = frequencyOne;
  audioNodeOne.connect(audioCtx.destination);
  audioNodeOne.start();

  const audioNodeTwo = audioCtx.createOscillator();
  audioNodeTwo.type = 'square';
  audioNodeTwo.frequency.value = frequencyTwo;
  audioNodeTwo.connect(audioCtx.destination);
  audioNodeTwo.start();

  await new Promise(res => setTimeout(
    () => {
      audioNodeOne.stop();
      audioNodeTwo.stop();
      res();
    }, duration))
};

export const playDTMF = async (input: String) => {
  switch(input) {
    case '0': {
      await playDTMFSound(1336, 941);
      break;
    }
    case '1': {
      await playDTMFSound(1209, 647);
      break;
    }
    case '2': {
      await playDTMFSound(1336, 697);
      break;
    }
    case '3': {
      await playDTMFSound(1477, 697);
      break;
    }
    case '4': {
      await playDTMFSound(1209, 770);
      break;
    }
    default: {
      await playDTMFSound(1477, 941);
      break;
    }
  }
};
