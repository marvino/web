import { z } from 'zod';

export const AppEntrySchema = z.object({
  repoUrl: z.string().url(),
  name: z.string(),
  menu: z.string(),
  author: z.string().optional(),
  description: z.string().optional(),
  downloadUrl: z.string().url(),
  version: z.number(),
  commit: z.string(),
  stars: z.number().default(0),
  flow3rSeed: z.string().optional(),
});

export const AppListSchema = z.object({
  isodatetime: z.string().datetime(),
  apps: z.array(AppEntrySchema)
});

export const parseAppListJSON = (json: any) => {
  const result = AppListSchema.safeParse(json);

  if (result.success) {
    return result.data;
  } else {
    // Handle parsing errors...
    console.error(result.error);
    return null;
  }
};

export type AppListType = z.infer<typeof AppListSchema>;
export type AppEntryType = z.infer<typeof AppEntrySchema>;
