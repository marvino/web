import { Component } from "solid-js"
import flowerImage from "../assets/flower.png"

export const LoadingSpinner: Component = (props) => {
    return (
        <div class="flex flex-col items-center gap-5 m-10">
            <style>
                {`
                    .loading-text::after {
                        content: "";
                        animation: dots-appear 2s infinite linear;
                    }

                    @keyframes dots-appear {
                        0% {
                            content: "";
                        }
                        25% {
                            content: ".";
                        }
                        50% {
                            content: "..";
                        } 
                        75% {
                            content: "...";
                        }
                    }
                `}
            </style>
            <img src={flowerImage} class="spinner" />
            <span class="text-push-pink text-2xl loading-text">Loading</span>
        </div>
    )
}
