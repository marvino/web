import { Component, For, createSignal, onMount } from "solid-js"
import { AppEntryType, AppListType } from "../backendSchema"
import { A } from "@solidjs/router"

const AppEntry: Component<{ app: AppEntryType }> = (props) => {
    return (
        <div class="w-90 flex h-full flex-col rounded-lg border-2 border-solid border-neutral-500 p-5">
            <h2 class="w-100 text-center font-title text-3xl">
                <i class="fa-brands fa-codepen mr-2"></i>&nbsp; {props.app.name}
            </h2>
            <span class="my-1 w-full  text-center">
                <i class="fa-solid fa-star"></i> {props.app.stars} <br />
            </span>
            <p class="flex-1 text-lg">
                Author: {props.app.author ? props.app.author : "<missing>"}
                <br />
                Description:{" "}
                {props.app.description
                    ? props.app.description
                    : "<no description>"}
                <br />
                Menu Type: {props.app.menu} <br />
                Version: {props.app.version} <br />
                Commit: {props.app.commit.substring(0, 12)}...
            </p>
            {/* Download Button */}
            <div class="mt-8 flex w-full flex-col sm:flex-row sm:gap-1">
                <a
                    class="btn flex-1 text-center dark:text-white "
                    href={props.app.repoUrl}
                >
                    <i class="fa-solid fa-code mr-2"></i>
                    Browse repo
                </a>
            </div>
            {/* Install with badge */}
            <A
                class="btn btn-secondary mt-2 text-center dark:text-black"
                href={`/apps/app?id=${props.app.repoUrl}`}
            >
                <i class="fa-solid fa-microchip mr-2"></i>
                Install
            </A>
        </div>
    )
}

const AppList: Component<{ data: AppListType }> = (props) => {
    const [appList, setAppList] = createSignal(props.data.apps)

    onMount(() => {
        if (
            !/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(
                navigator.userAgent
            )
        ) {
            ;(
                document.querySelector("#searchField") as HTMLInputElement
            ).focus()
        }
    })

    const applyTextFilter = (filterText: string) => {
        if (!filterText || filterText == "") {
            return props.data.apps
        }
        return props.data.apps.filter(
            (appElement) =>
                (
                    appElement.name.toLowerCase() +
                    appElement.description?.toLowerCase() +
                    appElement.author?.toLowerCase()
                ).indexOf(filterText) >= 0
        )
    }

    const applySort = (
        currentAppList: AppEntryType[],
        sortType: keyof AppEntryType
    ) => {
        currentAppList.sort((elementA, elementB) => {
            if (elementA && elementB) {
                const valueA = elementA[sortType]
                const valueB = elementB[sortType]

                if (typeof valueA == "number" && typeof valueB == "number") {
                    return valueB - valueA
                }
                return (valueA as "string").localeCompare(valueB as "string")
            }
            return -1
        })
        return [...currentAppList]
    }

    return (
        <div class="flex flex-col items-center gap-4">
            <div class="flex flex-col items-center gap-4 p-2 sm:flex-row">
                <div class="rounded-lg border-2 border-solid border-neutral-500 p-2 focus-within:outline focus-within:outline-go-green ">
                    <input
                        class="bg-transparent outline-none"
                        onInput={(input) => {
                            setAppList(applyTextFilter(input.target.value))
                        }}
                        id="searchField"
                    />
                    <i class="fa-solid fa-search"></i>
                </div>
                <div class="relative flex items-center gap-2 rounded-lg border-2 border-solid border-neutral-500 p-2 focus-within:outline focus-within:outline-go-green">
                    <select
                        class="bg-transparent outline-none"
                        onchange={(e) =>
                            setAppList(
                                applySort(
                                    appList(),
                                    e.target.value as
                                        | "name"
                                        | "menu"
                                        | "stars"
                                        | "version"
                                )
                            )
                        }
                    >
                        <option value="name">Name</option>
                        <option value="stars">Gitlab Stars</option>
                        <option value="version">Version</option>
                        <option value="menu">Menu Type</option>
                    </select>
                    <i class="fa-solid fa-sort-amount-desc"></i>
                </div>
            </div>
            <ul class="grid-cols-auto-fit-80 mb-10 grid w-full flex-1 auto-rows-min gap-4 px-4 sm:grid-cols-auto-fit-100">
                <For each={appList()}>{(app) => <AppEntry app={app} />}</For>
            </ul>
        </div>
    )
}

export default AppList
