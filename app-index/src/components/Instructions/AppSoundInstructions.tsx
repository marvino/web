import { Component } from "solid-js"
import AppSound from "./AppSound"

const AppSoundInstructions: Component<{
    appName: string
    appCode: string
}> = (props) => {
    return (
        <div class="flex flex-col items-center gap-4">
            <div class="mx-8 space-y-4 text-lg">
                <p class="text-center text-push-pink">
                    <i class="fa-full fa-exclamation m-2 "></i>
                    This install method is not yet supported but intended to be
                    developed. If you want to help setting it up in the batch
                    firmware hit us up in the batch tent.
                    <i class="fa-full fa-exclamation m-2"></i>
                </p>
            </div>
            <AppSound seed={props.appCode} />
        </div>
    )
}

export default AppSoundInstructions
