import {
    Show,
    Suspense,
    createResource,
    createSignal,
    type Component,
} from "solid-js"

import AppList from "../components/AppList"
import { ErrorPlaceholder } from "../components/ErrorPlaceholder"
import { LoadingSpinner } from "../components/LoadingSpinner"
import { ShameList } from "../components/ShameList"
import { fetchAppList, fetchShameList } from "../helpers/api"

const Home: Component = () => {
    const [appListData] = createResource(fetchAppList)
    const [shameListData] = createResource(fetchShameList)

    const [showShameList, setShowShameList] = createSignal(false)
    const toggleShameListVisible = () => setShowShameList(!showShameList())

    return (
        <div class="flex min-h-screen flex-col items-center dark:bg-black dark:text-white">
            <div>
                <h1 class="m-5 text-center font-titleBold text-4xl">
                    flow3r App Directory
                </h1>
            </div>
            <div class="mb-5 flex flex-col gap-4 sm:flex-row">
                <a
                    class="btn btn-accent"
                    href="https://git.flow3r.garden/flow3r/flow3r-apps"
                >
                    <i class="fa-solid fa-book mr-2"></i>
                    How to publish your app
                </a>
                <button class="btn btn-accent" onClick={toggleShameListVisible}>
                    <i class="fa-solid fa-dumpster-fire mr-2"></i>
                    Your app is missing?
                </button>
            </div>
            <Suspense fallback={<LoadingSpinner />}>
                <Show when={showShameList()}>
                    <Show
                        when={shameListData()}
                        fallback={<ErrorPlaceholder />}
                    >
                        {(shameListData) => (
                            <ShameList data={shameListData()} />
                        )}
                    </Show>
                </Show>
                <Show when={appListData()} fallback={<ErrorPlaceholder />}>
                    {(appListData) => <AppList data={appListData()} />}
                </Show>
            </Suspense>
            {/* Footer */}
            <p class="mb-3 text-center">
                Made with ❤️ at the badge tent
                <br />
                <a href="https://git.flow3r.garden/flow3r/web">
                    Contribute here
                </a>
                Last updated at {appListData()?.isodatetime}
                <br />
            </p>
        </div>
    )
}

export default Home
