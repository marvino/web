/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {
            fontFamily: {
                sans: ["Questrial", ...defaultTheme.fontFamily.sans],
                title: ["Beon"],
                titleBold: ["SairaStencilOne"],
            },
            gridTemplateColumns: {
                "auto-fill-100": "repeat(auto-fill, minmax(100px, 1fr))",
                "auto-fit-100": "repeat(auto-fit, minmax(400px, 1fr))",
            },
            colors: {
                "push-pink": "#FB48C4",
                "go-green": "#3FFF21",
            },
        },
    },
    plugins: [],
}
