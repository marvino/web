import { ESPLoader, FlashOptions, IEspLoaderTerminal, LoaderOptions, Transport } from "esptool-js"


let device: SerialPort | null = null;
let transport: Transport;
let chip: string | null = null;
let esploader: ESPLoader;
let terminal: IEspLoaderTerminal;

let connectButton: HTMLButtonElement;
let flashButton: HTMLButtonElement;
let releaseList: HTMLSelectElement;
let spinner: HTMLImageElement;
let contentContainer: HTMLDivElement;
let errorContainer: HTMLDivElement;
let fatalErrorContainer: HTMLDivElement;

let releases: Release[] = []
let fileArray: any[] = []
let isBusy = false;
let error: string | null = null;
let fatalError: string | null = null;

const manifestUrl = "/api/releases.json";

document.addEventListener("DOMContentLoaded", function () {
    terminal = createTerminal(document.getElementById("console")!)

    connectButton = document.getElementById("connect") as HTMLButtonElement;
    connectButton.onclick = connect;

    flashButton = document.getElementById("flash") as HTMLButtonElement;
    flashButton.onclick = flashFull;

    releaseList = document.getElementById("release-list") as HTMLSelectElement;
    releaseList.onchange = updateUi;

    spinner = document.getElementById("spinner") as HTMLImageElement;

    contentContainer = document.getElementById("content") as HTMLDivElement;
    errorContainer = document.getElementById("error") as HTMLDivElement;
    fatalErrorContainer = document.getElementById("fatal-error") as HTMLDivElement;

    if (!navigator.serial) {
        fatalError = "Incompatible browser. Please use a browser with WebSerial support (e.g. Chrome)";
        updateUi();
        return;
    }

    updateUi();

    loadReleases();
});

function updateUi() {
    if (fatalError != null) {
        contentContainer.style.display = "none";
        fatalErrorContainer.style.display = "block";
        fatalErrorContainer.innerText = fatalError;

        return;
    }

    if (error != null) {
        errorContainer.style.display = "block";
        errorContainer.innerText = error;

        isBusy = false;
        connectButton.disabled = true;
        flashButton.disabled = true;
        return;
    }


    if (isBusy) {
        spinner.style.display = "block";
    } else {
        spinner.style.display = "none";
    }

    connectButton.disabled = isBusy || chip != null;

    flashButton.disabled = isBusy || chip == null || releaseList.value === "";
}

function onError(e: any): void {
    if (e.message) {
        terminal.writeLine(`Error: ${e.message}`);
        console.error(e.message);
    } else {
        console.error(e);
    }

    error = "Something went wrong. Check the console for details.";
    updateUi();
}

function setBusy(value: boolean): void {
    isBusy = value;
    updateUi();
}

function createTerminal(element: HTMLElement): IEspLoaderTerminal {
    return {
        clean() {
            element.innerText = "";
            element.scrollTop = element.scrollHeight;
        },
        writeLine(data: string) {
            console.log(data);
            element.innerText = element.innerText.concat(data + "\n");
            element.scrollTop = element.scrollHeight;
        },
        write(data: string) {
            console.log(data);
            element.innerText = element.innerText.concat(data);
            element.scrollTop = element.scrollHeight;
        },
    };
}

async function connect(): Promise<void> {
    setBusy(true);

    if (device === null) {
        device = await navigator.serial.requestPort();
        transport = new Transport(device);
    }

    try {
        const options: LoaderOptions = {
            transport,
            baudrate: 460800,
            romBaudrate: 460800,
            terminal: terminal,
        };
        esploader = new ESPLoader(options);

        chip = await esploader.main_fn();
    } catch (e) {
        onError(e);
    } finally {
        setBusy(false);
    }
};

async function flashFull(): Promise<void> {
    setBusy(true);

    try {
        await downloadRelease()

        const flashOptions: FlashOptions = {
            fileArray,
            flashSize: "keep",
            eraseAll: true,
            compress: true,
            reportProgress: (fileIndex, written, total) => {
                console.log(`Flash progress: ${(written / total) * 100}`);
            },
        } as FlashOptions;
        await esploader.write_flash(flashOptions);
    } catch (e) {
        onError(e);
    } finally {
        setBusy(false);
    }
}

async function loadReleases() {
    setBusy(true);

    terminal.writeLine(`Loading manifest from ${manifestUrl}... `);

    const response = await fetch(manifestUrl);
    releases = await response.json() as Release[];

    // add dev release statically for now
    releases.push({
        name: "Development (latest commit)",
        partitions: [
            {
                name: "bootloader",
                url: "https://git.flow3r.garden/api/v4/projects/226/jobs/artifacts/main/raw/recovery/build/bootloader/bootloader.bin?job=build_recovery",
                offset: "0x0"
            },
            {
                name: "partition-table",
                url: "https://git.flow3r.garden/api/v4/projects/226/jobs/artifacts/main/raw/recovery/build/partition_table/partition-table.bin?job=build_recovery",
                offset: "0x8000"
            },
            {
                name: "recovery",
                url: "https://git.flow3r.garden/api/v4/projects/226/jobs/artifacts/main/raw/recovery/build/flow3r-recovery.bin?job=build_recovery",
                offset: "0x10000"
            },
            {
                name: "flow3r",
                url: "https://git.flow3r.garden/api/v4/projects/226/jobs/artifacts/main/raw/build/flow3r.bin?job=build",
                offset: "0x90000"
            }
        ]
    });

    Array.from(releaseList.children).map(x => x.remove());

    releases.forEach((release, index, _arr) => {
        const option = document.createElement("option");
        option.innerText = release.name;
        option.value = index.toString();
        releaseList.appendChild(option);
    });

    terminal.writeLine(`Found ${releases.length} releases`);

    terminal.writeLine("");
    terminal.writeLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    terminal.writeLine("!! Make sure your flow3r is in recovery or bootloader mode !!");
    terminal.writeLine("!!  (Hold down the left shoulder button when powering on)  !!");
    terminal.writeLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    setBusy(false);
}

async function downloadRelease(): Promise<void> {
    const release = releases[parseInt(releaseList.value, 10)];

    terminal.writeLine(`\nDownloading release ${release.name}... `);
    for (const partition of release.partitions) {
        terminal.writeLine(`  Downloading ${partition.name}`);

        const response = await fetch(partition.url);

        const bstr = await blobToBinaryString(await response.blob());
        fileArray.push({
            data: bstr,
            address: Number(partition.offset),
        });
    }
    terminal.writeLine("DONE\n");

    updateUi();
}

// Debug only
async function blobToBase64(blob: Blob): Promise<string> {
    return new Promise((resolve, _) => {
        const reader = new FileReader();
        reader.onloadend = () => {
            // Assume Chrome ~115
            const res = reader.result as string

            // Remove data:application/octet-stream;base64,
            const b64 = res.substring(res.indexOf(',') + 1);
            resolve(b64);
        }
        reader.readAsDataURL(blob);
    });
}

async function blobToBinaryString(blob: Blob): Promise<string> {
    return new Promise((resolve, _) => {
        const reader = new FileReader();
        reader.onloadend = () => {
            resolve(reader.result as string);
        }
        reader.readAsBinaryString(blob);
    });
}
